import React, { Component } from 'react';
import './App.css';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import Home from './components/Home/Home';
import History from './components/History/History';
import { socket } from './utils';
class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      greeting: ''
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    // this.setState({ name: event.target.value });
  }

  handleSubmit(event) {
    // event.preventDefault();
    // fetch(`/api/greeting?name=${encodeURIComponent(this.state.name)}`)
    //   .then(response => response.json())
    //   .then(state => this.setState(state));
  }

  componentDidMount(){
    //socket.emit('')
  }

  render() {
    return (
      <div className="App">
       <Router>
         <Switch>
          <Route path="/" exact>
            <Home />
          </Route>
          <Route path="/history" >
            <History />
          </Route>
        </Switch>
        </Router>   
      </div>
    );
  }
}

export default App;
