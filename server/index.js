const express = require('express');
const pino = require('express-pino-logger')();
const server = require('http').createServer(express);
const io = require('socket.io')(server , { cors: { origin: '*'}});
const TimerMinutes = 3000;

let interal;

const API_ENDPOINT = 'https://blockchain.info/tobtc?currency=USD&value=1';



const getApiAndEmit = socket => {
  const response = new Date();
  // Emitting a new message. Will be consumed by the client
  socket.emit("FromAPI", response);
};


io.on('connection', socket =>{
      console.log('user connected: ' + socket.id);
      if(! interal){
        interal  = global.setInterval(() => getAndEmit(socket) , TimerMinutes);
      }

      socket.on('getlistHistory' , data =>{
            //send history...
      });


      socket.on("disconnect" , () =>{
            global.clearInterval(interal);    
      })
      

});


const app = express();
app.use(pino);

app.get('/api/greeting', (req, res) => {
  const name = req.query.name || 'World';
  res.setHeader('Content-Type', 'application/json');
  res.send(JSON.stringify({ greeting: `Hello ${name}!` }));
});



const PORT = process.env.PORT || 3001;
app.listen(PORT, () =>{
  console.log(`Express server is running on localhost:${PORT}`);
});

//app.on('close')
app.on('listening', function () {
    global.setTimeout()  
  // do setTimeout
});


